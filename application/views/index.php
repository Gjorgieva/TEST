<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>List of employees</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
	<link href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container">
	<?php if (!isset($error)){?>
		 <div class="search-results-container col-centered">
			<h3 class="title-wrapper ml40">List of Emplyess</h3><hr/>
			<?php 
			foreach ($user as $arr) { ?><!--loop through  employee list get from controller and display all data related with the employee-->
			<div class="list-wrapper row">
				<div class="col-md-1">
				  <img class="lazy-image loaded" alt="<?php echo $arr->name?>" height="56" width="56"  src="<?php echo $arr->avatar ?> ">
				</div>
				<div class="col-md-6">
					<span class="name-and-icon"><?php echo $arr->name?></span>	- <span><?php echo $arr->company?></span>
					<p class="job-title"><?php echo $arr->title?></p>
					<p class="bio"><?php echo $arr->bio ?></p>
				</div>
			</div>
		 <?php } ?>
		</div>	
		<?php  } else {?>
		<div class="alert alert-danger mt20">
			<strong>Error!</strong> Something went wrong!
		</div>
		<?php }?>
	</div>
</body>
</html>